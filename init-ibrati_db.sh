bash
#!/bin/bash
set -e

# Wait for SQL Server to start
until /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "Teste01#" -Q "SELECT 1" &> /dev/null
do
  echo "Waiting for SQL Server to start..."
  sleep 1
done

# Run the setup script to create the DB and the schema in the DB
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Teste01# -d master -i /sql-scripts/create-ibrati_digital_db.sql
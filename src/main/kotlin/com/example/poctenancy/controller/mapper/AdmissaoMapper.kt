package com.example.poctenancy.controller.mapper

import com.example.poctenancy.controller.dto.AdmissaoRequest
import com.example.poctenancy.domain.Admissao

fun AdmissaoRequest.toDomain() = Admissao(
    cpf = this.cpf,
    dataNascimento = this.dataNascimento,
    licenciado = this.licenciado,
)

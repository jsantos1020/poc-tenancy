package com.example.poctenancy.controller.mapper

import com.example.poctenancy.config.security.JwtTokenProvider
import com.example.poctenancy.controller.dto.AutenticacaoRequestData
import com.example.poctenancy.service.AdmissaoService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

@RestController
@RequestMapping("/v1/authenticate")
class AuthController(
    private val admissaoService: AdmissaoService,
    private val jwtTokenProvider: JwtTokenProvider,
) {

    @PostMapping
    fun authenticate(@RequestBody request: AutenticacaoRequestData): ResponseEntity<String> {
        val admissao = admissaoService.authenticate(
            cpf = request.cpf,
            dataNascimento = converterStringParaLocalDate(dataString = request.password, formato = "yyyy-MM-dd")!!,
        ) ?: error("error parse")

        val token = jwtTokenProvider.createToken(admissao = admissao)

        return ResponseEntity.ok(token)
    }

    fun converterStringParaLocalDate(dataString: String, formato: String): LocalDate? {
        try {
            val formatter = DateTimeFormatter.ofPattern(formato)
            return LocalDate.parse(dataString, formatter)
        } catch (e: DateTimeParseException) {
            // Tratar caso o formato da data seja inválido
            return null
        }
    }
}

package com.example.poctenancy.controller

import com.example.poctenancy.domain.Origens
import com.example.poctenancy.service.OrigensService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/origens")
class OrigensController(private val service: OrigensService) {

    @GetMapping
    fun findAll(): List<Origens> {
        val response = service.findAll() ?: emptyList()

        return response
    }
}

package com.example.poctenancy.controller.dto

import java.io.Serializable
import java.time.LocalDate

data class AdmissaoRequest(
    val cpf: String,
    val dataNascimento: LocalDate,
    val licenciado: String,
) : Serializable

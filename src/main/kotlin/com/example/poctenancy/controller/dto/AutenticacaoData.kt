package com.example.poctenancy.controller.dto

data class AutenticacaoRequestData(
    val cpf: String,
    val password: String,
)

package com.example.poctenancy.controller

import com.example.poctenancy.controller.dto.AdmissaoRequest
import com.example.poctenancy.controller.mapper.toDomain
import com.example.poctenancy.service.AdmissaoService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/admissoes")
class AdmissaoController(private val service: AdmissaoService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody request: AdmissaoRequest) {
        service.save(request.toDomain())
    }
}

package com.example.poctenancy.service

import com.example.poctenancy.domain.Origens
import com.example.poctenancy.repository.OrigensRepository
import org.springframework.stereotype.Service

@Service
class OrigensService(private val repository: OrigensRepository) {

    fun findAll(): List<Origens>? = repository.findByAll()
}

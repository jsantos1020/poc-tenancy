package com.example.poctenancy.service

import com.example.poctenancy.domain.Admissao
import com.example.poctenancy.repository.AdmissaoRepository
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class AdmissaoService(private val repository: AdmissaoRepository) {

    fun save(admissao: Admissao) {
        repository.save(admissao = admissao)
    }

    fun authenticate(cpf: String, dataNascimento: LocalDate): Admissao? {
        return repository.findByCpfAndDataNascimento(cpf = cpf, dataNascimento = dataNascimento)
    }
}

package com.example.poctenancy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PocTenancyApplication

fun main(args: Array<String>) {
    runApplication<PocTenancyApplication>(*args)
}

package com.example.poctenancy.domain

data class Origens(
    val id: Int,
    val origem: String,
    val licenciado: String,
)

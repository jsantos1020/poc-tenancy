package com.example.poctenancy.domain

import java.time.LocalDate
import java.util.UUID

data class Admissao(
    val id: UUID = UUID.randomUUID(),
    val cpf: String,
    val dataNascimento: LocalDate,
    val licenciado: String,
)

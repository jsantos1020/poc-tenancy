package com.example.poctenancy.repository

import com.example.poctenancy.domain.Origens
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet

@Repository
class OrigensRepository(private val namedParameterJdbcTemplate: NamedParameterJdbcTemplate) {

    fun findByAll(): List<Origens>? {
        val querySql = """
            SELECT ID, ORIGEM, LICENCIADO 
            FROM ORIGENS
        """.trimIndent()

        return try {
            namedParameterJdbcTemplate.query(
                querySql,
            ) { rs: ResultSet, _: Int ->
                Origens(
                    id = rs.getInt("ID"),
                    origem = rs.getString("ORIGEM"),
                    licenciado = rs.getString("LICENCIADO"),
                )
            }
        } catch (e: EmptyResultDataAccessException) {
            null
        }
    }
}

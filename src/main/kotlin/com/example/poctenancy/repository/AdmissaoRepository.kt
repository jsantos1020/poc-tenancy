package com.example.poctenancy.repository

import com.example.poctenancy.domain.Admissao
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.time.LocalDate
import java.util.*

@Repository
class AdmissaoRepository(private val namedParameterJdbcTemplate: NamedParameterJdbcTemplate) {

    private val insertAdmissaoSql = """
        INSERT INTO ADMISSOES (ID, CPF, DATA_NASCIMENTO, LICENCIADO) 
        VALUES (:id, :cpf, :dataNascimento, :licenciado)
    """.trimIndent()

    fun save(admissao: Admissao) {
        val parameters = MapSqlParameterSource()
        parameters.addValue("id", admissao.id)
        parameters.addValue("cpf", admissao.cpf)
        parameters.addValue("dataNascimento", admissao.dataNascimento)
        parameters.addValue("licenciado", admissao.licenciado)

        namedParameterJdbcTemplate.update(insertAdmissaoSql, parameters)
    }

    fun findByCpfAndDataNascimento(cpf: String, dataNascimento: LocalDate): Admissao? {
        val querySql = """
        SELECT ID, CPF, DATA_NASCIMENTO, LICENCIADO 
        FROM ADMISSOES 
        WHERE CPF = :cpf AND DATA_NASCIMENTO = :dataNascimento
        """.trimIndent()

        val parameters = MapSqlParameterSource()
        parameters.addValue("cpf", cpf)
        parameters.addValue("dataNascimento", dataNascimento)

        return try {
            namedParameterJdbcTemplate.queryForObject(
                querySql,
                parameters,
            ) { rs, _ ->
                Admissao(
                    id = UUID.fromString(rs.getString("ID")),
                    cpf = rs.getString("CPF"),
                    dataNascimento = rs.getDate("DATA_NASCIMENTO").toLocalDate(),
                    licenciado = rs.getString("LICENCIADO"),
                )
            }
        } catch (e: EmptyResultDataAccessException) {
            null
        }
    }
}

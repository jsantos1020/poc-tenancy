package com.example.poctenancy.config

import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.JdbcTemplate
import javax.sql.DataSource

@Configuration
class DataSourceConfig {

    @Bean
    fun admissaoDataSource(): DataSource {
        return DataSourceBuilder.create()
            .url("jdbc:sqlserver://localhost:1433;databaseName=Admissao_Digital;encrypt=true;trustServerCertificate=true")
            .username("admissao_app")
            .password("AdmissaoApp@")
            .driverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
            .build()
    }

    @Bean
    fun bestwayDataSource(): DataSource {
        return DataSourceBuilder.create()
            .url("jdbc:sqlserver://localhost:1433;databaseName=Bestway;encrypt=true;trustServerCertificate=true")
            .username("bestway_app")
            .password("BestWayApp@")
            .driverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
            .build()
    }

    @Bean
    fun ibratiDataSource(): DataSource {
        return DataSourceBuilder.create()
            .url("jdbc:sqlserver://localhost:1433;databaseName=Ibrati;encrypt=true;trustServerCertificate=true")
            .username("ibrati_app")
            .password("IbratiApp@")
            .driverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
            .build()
    }

    @Bean
    fun dataSource(): DataSource {
        val routingDataSource = MultitenantDataSource()
        val targetDataSources = HashMap<Any, Any>()
        targetDataSources["Admissao_Digital"] = admissaoDataSource()
        targetDataSources["Bestway"] = bestwayDataSource()
        targetDataSources["Ibrati"] = ibratiDataSource()

        routingDataSource.setTargetDataSources(targetDataSources)
        routingDataSource.setDefaultTargetDataSource(admissaoDataSource())

        return routingDataSource
    }

    @Bean
    fun jdbcTemplate(dataSource: DataSource): JdbcTemplate {
        return JdbcTemplate(dataSource)
    }
}

package com.example.poctenancy.config

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource

class MultitenantDataSource : AbstractRoutingDataSource() {
    override fun determineCurrentLookupKey(): Any? {
        return TenantContext.getCurrentTenant()
    }
}

object TenantContext {
    private val currentTenant = ThreadLocal<String>()

    fun setCurrentTenant(tenantId: String) {
        currentTenant.set(tenantId)
    }

    fun getCurrentTenant(): String? {
        return currentTenant.get()
    }

    fun clear() {
        currentTenant.remove()
    }
}

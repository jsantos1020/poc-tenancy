package com.example.poctenancy.config

import org.flywaydb.core.Flyway
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource

@Configuration
class FlywayConfig {

    // Supondo que você tenha um DataSource para cada tenant
    @Bean
    fun admissaoFlyway(admissaoDataSource: DataSource): Flyway {
        return Flyway.configure()
            .dataSource(admissaoDataSource)
            // Configure outras propriedades do Flyway conforme necessário
            .load()
    }

    // Repita para os outros tenants (Bestway e Ibrati), por exemplo:
    // @Bean
    // fun bestwayFlyway(bestwayDataSource: DataSource): Flyway { ... }

    // @Bean
    // fun ibratiFlyway(ibratiDataSource: DataSource): Flyway { ... }

    @Bean
    fun flywayRunner(
        admissaoFlyway: Flyway,
        // , bestwayFlyway: Flyway, ibratiFlyway: Flyway
    ): CommandLineRunner {
        return CommandLineRunner {
            // Execute as migrações para cada tenant
            admissaoFlyway.migrate()
            // bestwayFlyway.migrate()
            // ibratiFlyway.migrate()
        }
    }
}

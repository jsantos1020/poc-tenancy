package com.example.poctenancy.config.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
class SecurityConfig(
    private val tenantAuthenticationFilter: TenantAuthenticationFilter,
) {

    @Bean
    fun defaultSecurityFilterChain(http: HttpSecurity): SecurityFilterChain {
        http
            .csrf { it.disable() } // Desabilita CSRF para APIs stateless
            .authorizeHttpRequests { authorize ->
                authorize
                    .requestMatchers("/v1/admissoes", "/v1/authenticate").permitAll() // Permite acesso aos endpoints
                    .anyRequest().authenticated() // Todos os outros endpoints requerem autenticação
            }
            .httpBasic { httpBasic -> httpBasic } // Utiliza autenticação básica HTTP
            .addFilterBefore(tenantAuthenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
        return http.build()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }
}

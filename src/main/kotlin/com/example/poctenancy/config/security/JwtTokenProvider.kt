package com.example.poctenancy.config.security

import com.example.poctenancy.domain.Admissao
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import java.util.*

@Component
class JwtTokenProvider(private val keyReader: KeyReader) {

    fun createToken(admissao: Admissao): String {
        val now = Date()
        val doisMesesEmMilissegundos = 2 * 30 * 24 * 60 * 60 * 1000L
        val validity = Date(now.time + doisMesesEmMilissegundos) // 2 meses de validade

        return Jwts.builder()
            .setSubject(admissao.cpf)
            .claim("licenciado", admissao.licenciado)
            .setIssuedAt(now)
            .setExpiration(validity)
            .signWith(keyReader.keyPair.private, SignatureAlgorithm.RS256)
            .compact()
    }

    fun getAuthentication(token: String): Authentication {
        val claims = Jwts.parserBuilder()
            .setSigningKey(keyReader.keyPair.public)
            .build()
            .parseClaimsJws(token)
            .body

        val authorities = (claims.get("auth", List::class.java) ?: emptyList<String>())
            .map { SimpleGrantedAuthority(it as String) }

        return UsernamePasswordAuthenticationToken(claims.subject, token, authorities)
    }

    fun validateToken(token: String): Boolean {
        try {
            Jwts.parserBuilder().setSigningKey(keyReader.keyPair.public).build().parseClaimsJws(token)
            return true
        } catch (e: Exception) {
            println("error parse token")
        }
        return false
    }

    fun getClaimFromToken(token: String, claimKey: String): String? {
        val claims = getAllClaimsFromToken(token = token)
        return claims[claimKey]?.toString()
    }

    private fun getAllClaimsFromToken(token: String): Claims {
        return Jwts.parserBuilder()
            .setSigningKey(keyReader.keyPair.public)
            .build()
            .parseClaimsJws(token)
            .body
    }
}

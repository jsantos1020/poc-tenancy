package com.example.poctenancy.config.security

import com.example.poctenancy.config.TenantContext
import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter

@Component
class TenantAuthenticationFilter(private val jwtTokenProvider: JwtTokenProvider) : OncePerRequestFilter() {
    companion object {
        private const val TENANT_ID_DEFAULT = "Admissao_Digital"
        private const val AUTHORIZATION_HEADER = "Authorization"
        private const val BEARER_PREFIX = "Bearer "
        private const val CLAIM_KEY_LICENCIADO = "licenciado"
    }

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain,
    ) {
        val path = request.requestURI

        if (isPublicEndpoint(path)) {
            setTenant(tenantId = TENANT_ID_DEFAULT)
            filterChain.doFilter(request, response)
            return
        }

        val jwt = resolveToken(request)
        if (jwt != null && jwtTokenProvider.validateToken(jwt)) {
            val tenantIdClaim = jwtTokenProvider.getClaimFromToken(jwt, CLAIM_KEY_LICENCIADO)
            val auth = jwtTokenProvider.getAuthentication(jwt)
            SecurityContextHolder.getContext().authentication = auth

            try {
                setTenant(tenantId = tenantIdClaim ?: TENANT_ID_DEFAULT)
                filterChain.doFilter(request, response)
            } finally {
                TenantContext.clear()
                SecurityContextHolder.clearContext()
            }
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid or missing token")
        }
    }

    private fun isPublicEndpoint(path: String): Boolean =
        path.startsWith("/v1/admissoes") || path.startsWith("/v1/authenticate")

    private fun setTenant(tenantId: String) {
        TenantContext.setCurrentTenant(tenantId = tenantId)
    }

    private fun resolveToken(request: HttpServletRequest): String? {
        val bearerToken = request.getHeader(AUTHORIZATION_HEADER)
        return bearerToken?.takeIf { it.startsWith(BEARER_PREFIX) }?.substring(BEARER_PREFIX.length)
    }
}

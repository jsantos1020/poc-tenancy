package com.example.poctenancy.config.security

import org.bouncycastle.util.io.pem.PemReader
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStreamReader
import java.security.KeyFactory
import java.security.KeyPair
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec

@Component
class KeyReader(
    @Value("\${jwt.validation.key}") publicKeyAsString: String,
    @Value("\${jwt.sign.key}") privateKeyAsString: String,
) {

    val keyPair: KeyPair

    init {
        val keyFactory = KeyFactory.getInstance("RSA")

        val keySpecPKCS8 = PKCS8EncodedKeySpec(loadKey(privateKeyAsString))
        val privateKey = keyFactory.generatePrivate(keySpecPKCS8) as RSAPrivateKey

        val keySpecX509 = X509EncodedKeySpec(loadKey(publicKeyAsString))
        val publicKey = keyFactory.generatePublic(keySpecX509) as RSAPublicKey

        this.keyPair = KeyPair(publicKey, privateKey)
    }

    private fun loadKey(key: String): ByteArray {
        try {
            PemReader(InputStreamReader(ByteArrayInputStream(key.toByteArray()))).use { pemReader -> return pemReader.readPemObject().content }
        } catch (e: IOException) {
            throw RuntimeException("Failed to read private key: $e")
        }
    }
}

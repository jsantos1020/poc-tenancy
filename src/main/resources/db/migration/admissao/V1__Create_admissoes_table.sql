CREATE TABLE ADMISSOES (
                           ID UNIQUEIDENTIFIER PRIMARY KEY,
                           CPF NVARCHAR(14) NOT NULL,
                           DATA_NASCIMENTO DATE,
                           LICENCIADO NVARCHAR(255)
);